<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrudmenuController extends Controller
{
   public function index(Request $request)
   {
   	
       if($request->has('search')){
            $menu = menuController::where('nama', '%'.$request->search.'%')->get();
        } else {
             $data_menu = \App\models\menu::all();
        }

      $data_menu = \App\models\menu::all();
   	return view('crud.insert',['data_menu' => $data_menu]);
   }
   
   public function create(Request $request)
   {
   		\App\models\menu::create($request->all());
   		return redirect('/menucrud')->with('sukses','Data Berhasil Diinput');
   }

   public function edit($id)
   {
   	 $menucrud = \App\models\menu::find($id);
   	 return view('crud/edit',['menu' => $menucrud]);
   }

   public function update(Request $request, $id)
   {
   		 $menucrud = \App\models\menu::find($id);
   		 $menucrud->update($request->all());
   		 return redirect('/menucrud') -> with('sukses','Data Berhasil DiUpdate');
   }

   public function delete($id)
   {
   	$menucrud = \App\models\menu::find($id);
   	$menucrud->delete($menucrud);
   	return redirect('/menucrud')->with('sukses','Data Berhasil Dihapus');
   }
}

