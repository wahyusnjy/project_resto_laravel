const dataMakanan =[
{
	foto:"img/mieayam.jpeg", 
	nama: 'Mie ayam',
	desc:'Mie rebus yang khas dicampur dengan ayam',
	harga: "Rp16.000"
}, 
{
	foto:"img/baksourat.jpg", 
	nama: 'Bakso Urat',
	desc:'Baksonya berurat', 
	harga: "Rp20.000"
}, 
{
	foto:"img/aqua.jfif",
	nama:'Aqua',
	desc:'Air mineral',
	harga:"Rp2000"
},
{
	foto:"img/bakso.jpg", 
	nama: 'Bakso biasa', 
	desc:'Baksonya ironmen',
	harga: "Rp15.000"
}, 
{
	foto:"img/makanan6.jpg", 
	nama: 'Nasi goreng',
	desc:'Nasinya Diongseng', 
	harga: "Rp15.000"
},  
{
	foto:"img/ayamgoyeng.jpg", 
	nama: 'Nasi Ayam',
	desc:'Nasi jeung ayam', 
	harga: "Rp10.000"
}, 
{
	foto:"img/mie.jpg", 
	nama: 'Mie Sehat',
	desc:'Mie sehat ala mang oleh', 
	harga: "Rp10.000"
},  
{
	foto:"img/seblak.jpg", 
	nama: 'Seblak Mercon',
	desc:'pacar anda ngambek? bisa beli ini',
	harga: "Rp8.000"
},  
{
	foto:"img/teh.jpg", 
	nama: 'Es_Teh', 
	desc:'Rasanya anjay banget',
	harga: "Rp4.500"
},   
{
 	foto:"img/escampur.jpg", 
 	nama: 'ABCD',
 	desc:'Es yang ada di film upin ipin', 
 	harga: "Rp10.000"
 },
 ];

const callbackMap = (item, index)=>{
  const elmnt = document.querySelector('.daftar-makanan');
  
  elmnt.innerHTML += `
    <div class="box-makanan ">
      <img src="${item.foto}" style="height:200px; width:300px;">
          <h5 class="card-title">${item.nama}</h5>
          <p class="card-text">${item.desc}</p>
          <h5 class="card-text">${item.harga}</h5>
          <a href="/form" class="btn button-pesan">Beli</a>>

        </div>
   `
 
 }

dataMakanan.map(callbackMap);


const buttonElemnt = document.querySelector('.button-search');

buttonElemnt.addEventListener('click', ()=>{
  const hasilPencarian = dataMakanan.filter((item, index)=>{
                const inputElemnt    =document.querySelector('.input-keyword');
                const buttonElemnt   = document.querySelector('.button-search');
                const namaItem       = item.nama.toLowerCase();
                const keyword      = inputElemnt.value.toLowerCase();

                return namaItem.includes(keyword);
        })

  document.querySelector('.daftar-makanan').innerHTML ='';

  hasilPencarian.map(callbackMap);
});

//reduce

const jumlahPesanan = []
const buttonPesan = document.querySelectorAll('.button-pesan');
const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

buttonPesan.forEach((item, index)=>{
item.addEventListener('click', ()=>{
	jumlahPesanan.push(1);
	
	
	const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
		return accumulator + currentValue;
		}, 0);


	elmntJumlahPesanan.innerHTML = hasil;
	})
})


