 <?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/menu', function(){
	return view('pages.menu2');
});

Route::get('/form', function(){
	return view('form.form');
});

Route::get('/ok', function(){
	return view('konfirmasi.konfirmasi');
});
Route::get('/admin', function(){
	return view('admin.admin');
});

Route::get('/menucrud','CrudmenuController@index');
Route::post('/menucrud/create','CrudmenuController@create');
Route::get('/menucrud/{id}/edit','CrudmenuController@edit');
Route::post('/menucrud/{id}/update','CrudmenuController@update');
Route::get('/menucrud/{id}/delete','CrudmenuController@delete');

