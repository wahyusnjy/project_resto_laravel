<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\menuController;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getMenu', [menuController::class, 'index']);