@extends('layouts.frontend.master')
@section('content')

<!-- awal carousel -->
    <div class="bd-example">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/makanan6.jpg" class="d-block " height="1055" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Gambar Slide Yang Pertama</h5>
              <p>Nasi goreng</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/mie.jpg" class="d-block " height="1025" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Gambar Slide Yang Kedua</h5>
              <p>Mie Sehat</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/ayamgoyeng.jpg" class="d-block " height="1000" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Gambar Slide Yang Ketiga</h5>
              <p>Ayam Goreng Dahlah</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
    
    <hr>
    <hr>
    <hr>
 <hr>
 <hr> 
@stop