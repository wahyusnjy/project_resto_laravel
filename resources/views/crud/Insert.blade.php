<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<nav class="navbar navbar-dark btn-secondary ">
    <nav class="navbar ">
      <a class="navbar-brand" href="/">
        Tangan Handal
      </a>
   <div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Menu
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/menu">Sarapan</a>
    <a class="dropdown-item" href="#">Mie ayam</a>
    <a class="dropdown-item" href="#">Bakso Urat</a>
    <a class="dropdown-item" href="#">Bakso Biasa</a>
    <a class="dropdown-item" href="#">Aqua</a>
    <a class="dropdown-item" href="#">Nasi Goreng</a>
    <a class="dropdown-item" href="#">Nasi Ayam</a>
    <a class="dropdown-item" href="#">mie sehat</a>
    <a class="dropdown-item" href="#">seblak</a>
    <a class="dropdown-item" href="#">Es teh</a>
    <a class="dropdown-item" href="#">Es_campur</a>
    <a class="dropdown-item" href="/">Home</a>
  </div>
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Catering
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Pernikahan</a>
    <a class="dropdown-item" href="#">Ultang tahun</a>
    <a class="dropdown-item" href="#">Arisan</a>
  </div>  
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Info
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="http://instagram.com/wahyuxtiramisu/?hl=id">Instagram</a>
    <a class="dropdown-item" href="#">Facebook</a>
    <a class="dropdown-item" href="#">Contact</a>
  </div>
</div>
        

        </nav>
 
</div>

</nav>
</head>
</head>
<body>
	<div class="container">
		@if(session('sukses'))
		<div class="alert alert-success" role="alert">
				  {{session('sukses')}}				
				</div>
				@endif
		<div class="row">
			<div class="col-6">
				<h1>Data Makanan</h1>
			</div>
			<div class="col-6">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
				  Tambah Data Makanan
				</button>

				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				       <form action="/menucrud/create" method="POST">
				       	{{csrf_field()}}
							  <div class="form-group">
							    <label for="exampleInputEmail1">Id</label>
							    <input name="id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="id">
							  </div>
							   <div class="form-group">
							    <label for="exampleInputEmail1">Nama</label>
							    <input name="Nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama">
							  </div>
							   <div class="form-group">
							    <label for="exampleFormControlTextarea1">Desc</label>
							    <textarea name="desc" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
							  </div>
							   <div class="form-group">
							    <label for="exampleInputEmail1">Harga</label>
							    <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Harga">
							  </div>
							  <div class="form-group">
							    <label for="exampleInputEmail1">Asset</label>
							    <input name="Asset" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Asset">
							  </div>
							 
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				         <button type="submit" class="btn btn-primary">Submit</button>
							</form>	
				      </div>
				    </div>
				  </div>
				</div>
			</div>
				
		<table class="table table-hover">
			<tr>
				<th>id</th>
				<th>Nama</th>
				<th>desc</th>
				<th>harga</th>
				<th>Asset</th>
				<th>Aksi</th>
			</tr>
			@foreach($data_menu as $menu)
			<tr>
				<td>{{$menu->id}}</td>
				<td>{{$menu->Nama}}</td>
				<td>{{$menu->desc}}</td>
				<td>{{$menu->harga}}</td>
				<td>{{$menu->Asset}}</td>
				<td>
					<a href="/menucrud/{{$menu->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
					<a href="/menucrud/{{$menu->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Di Hapus?')">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
		</div>
	</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
