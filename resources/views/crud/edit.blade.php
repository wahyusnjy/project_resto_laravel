@extends('layouts.master2')
@section('konten')

		<h1>Edit</h1>
		@if(session('sukses'))
		<div class="alert alert-success" role="alert">
				  {{session('sukses')}}				
				</div>
				@endif
		<div class="row">
			<div class="col-lg-12">
			<form action="/menucrud/{{$menu->id}}/update" method="POST">
				       	{{csrf_field()}}
							  <div class="form-group">
							    <label for="exampleInputEmail1">Id</label>
							    <input name="id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="id" value="{{$menu->id}}">

							  </div>
							   <div class="form-group">
							    <label for="exampleInputEmail1">Nama</label>
							    <input name="Nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama" value="{{$menu->Nama}}">

							  </div>
							   <div class="form-group">
							    <label for="exampleFormControlTextarea1">Desc</label>
							    <textarea name="desc" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{$menu->desc}}</textarea>
							  </div>

							   <div class="form-group">
							    <label for="exampleInputEmail1">Harga</label>
							    <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Harga" value="{{$menu->harga}}">
							  </div>

							  <div class="form-group">
							    <label for="exampleInputEmail1">Asset</label>
							    <input name="Asset" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Asset" value="{{$menu->Asset}}">
							  </div>
							<button type="submit" class="btn btn-warning">Update</button>
							</form>	
							</div>
				   		 </div>

@endsection