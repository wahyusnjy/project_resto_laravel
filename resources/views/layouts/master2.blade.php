<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	 <nav class="navbar navbar-dark btn-secondary ">
    <nav class="navbar ">
      <a class="navbar-brand" href="/">
        Tangan Handal
      </a>
   <div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Menu
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="menu.html">Sarapan</a>
    <a class="dropdown-item" href="#">Mie ayam</a>
    <a class="dropdown-item" href="#">Bakso Urat</a>
    <a class="dropdown-item" href="#">Bakso Biasa</a>
    <a class="dropdown-item" href="#">Aqua</a>
    <a class="dropdown-item" href="#">Nasi Goreng</a>
    <a class="dropdown-item" href="#">Nasi Ayam</a>
    <a class="dropdown-item" href="#">mie sehat</a>
    <a class="dropdown-item" href="#">seblak</a>
    <a class="dropdown-item" href="#">Es teh</a>
    <a class="dropdown-item" href="#">Es_campur</a>
    <a class="dropdown-item" href="/">Home</a>
  </div>
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Catering
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Pernikahan</a>
    <a class="dropdown-item" href="#">Ultang tahun</a>
    <a class="dropdown-item" href="#">Arisan</a>
  </div>  
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Info
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="http://instagram.com/wahyuxtiramisu/?hl=id">Instagram</a>
    <a class="dropdown-item" href="#">Facebook</a>
    <a class="dropdown-item" href="#">Contact</a>
  </div>
</div>
        <a class="btn btn-primary align-top" href="/form" role="button" >Order Now</a>

        </nav>
        <nav class="navbar ">
  <div class="search-box  form-inline">
    <input class="  form-control mr-sm-2 input-keyword" type="text" placeholder="Search" aria-label="Search" >
    <button class="btn btn-outline-success my-- my-sm-0 button-search" type="submit">Search</button>
</div>
</nav>
</nav>
</head>
</head>
<body>
	<div class="container">
@yield('konten')
	</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>