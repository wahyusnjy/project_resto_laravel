<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/style.css">
<head>
	<title>Home page</title>
<!-- Awal Navbar -->
<nav class="navbar navbar-dark btn-secondary fixed-top">
    <nav class="navbar ">
      <a class="navbar-brand" >
        <img src="img/logo.png" width="40" height="40" class="d-inline-block align-top" alt="">
        Tangan Handal
      </a>
   <div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Menu
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/menu">Sarapan</a>
    <a class="dropdown-item" href="#">Mie ayam</a>
    <a class="dropdown-item" href="#">Bakso Urat</a>
    <a class="dropdown-item" href="#">Bakso Biasa</a>
    <a class="dropdown-item" href="#">Aqua</a>
    <a class="dropdown-item" href="#">Nasi Goreng</a>
    <a class="dropdown-item" href="#">Nasi Ayam</a>
    <a class="dropdown-item" href="#">mie sehat</a>
    <a class="dropdown-item" href="#">seblak</a>
    <a class="dropdown-item" href="#">Es teh</a>
    <a class="dropdown-item" href="#">Es_campur</a>
  </div>
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Catering
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Pernikahan</a>
    <a class="dropdown-item" href="#">Ultang tahun</a>
    <a class="dropdown-item" href="#">Arisan</a>
  </div>  
</div>

<div class="dropdown">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Info
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="http://instagram.com/wahyuxtiramisu/?hl=id">Instagram</a>
    <a class="dropdown-item" href="#">Facebook</a>
    <a class="dropdown-item" href="#">Contact</a>
  </div>
</div>
        <a class="btn btn-primary align-top" href="form.html" role="button" >Order Now</a>

        </nav>

        <nav class="navbar ">
  <a href="/admin"><img src="img/person2.png" width="40" height="40"></a>
</nav>
      </nav>
      <!-- Akhir Navbar -->
</head>
<body>
  
@yield('content')
   <!-- Footer-->
<footer>
  <div class="container text-center">
    <div class="row">
      <div class="col-sm-12">
        <p>&copy;Copyright  2019 | built by. <a href="http://instagram.com/wahyuxtiramisu/?hl=id">Wahyu Sanjaya</a>. </p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <a href="https://www.youtube.com/channel/UCydmuo5shwgWKF5DVHAx7Jw" class="btn btn-danger">Subscribe My Channel</a>
      </div>
    </div>
  </div>
</footer>
<!-- Akhir footer -->

	
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>