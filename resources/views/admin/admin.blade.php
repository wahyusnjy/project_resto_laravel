<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

   <style>
      .container-admin{
        margin-top: 250px;
      }
      .menu{
        margin-top: 200px;
      }
      .btn-admin{
        margin-bottom: 30px
      }
    </style>
</head>
<body>
    <a class="back" href="/"><img src="img/arrowback.png" alt="arrow back" id="home" width="60" height="60"></a>
    <div class="container-admin container text-center">
        <h1>Admin</h1>
        <br/>
        <button type="button" class="btn-admin btn btn-danger btn-lg btn-block text-light"><a href="/menucrud" class="menu text-decoration-none text-reset"><h3>Menu</h3></a></button>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>